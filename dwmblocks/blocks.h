//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
  /*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
  //{"  ", "~/.local/bin/volume.sh",				0,		10},
  {"^b#a3be8c^^c#2e3440^", "~/.local/bin/volume.sh",				0,		10},
  //	{" ", "pacman -Q | wc -l",					120,		0},
  //	{" ", "df -h / | tail -1 | awk {'printf $3'}",			120,		0},
  //	{" ", "date '+%d/%m [%a]'",				60,		0},
  //{" ", "date '+%d/%m/%y [%a]'",				60,		0},
  {"^b#bf616a^^c#eceff4^  ", "date '+%d/%m/%y'",				60,		0},
  {"^b#ebcb8b^^c#2e3440^  ", "date '+%H:%M:%S '",				1,		0},
  //	{" ", "date '+%R '",				60,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;

